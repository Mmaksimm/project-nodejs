const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    constructor(repository) {
        this.repository = repository;
    }

    getUsers() {
        const users = this.repository.getAll();
        if (!users) throw new Error('Users not found');
        return users;
    }

    getUser(id) {
        const user = this.repository.getOne({ id });
        if (!user) throw new Error('User not found');
        return user;
    }

    createUser(userData) {
        const createdUser = this.repository.create(userData);
        if (!createdUser) throw new Error('User not created');
        return createdUser;
    }

    updateUser(id, userData) {
        if (!this.repository.getOne({ id })) {
            throw new Error("User can't be updated, because not found");
        };
        const updatedUser = this.repository.update(id, userData);
        if (!updatedUser) throw new Error('User not updated');
        return updatedUser;
    }

    deleteUser(id) {
        if (!this.repository.getOne({ id })) {
            throw new Error("User can't be deleted, because not found");
        };
        const deletedUser = this.repository.delete(id);
        if (!deletedUser) throw new Error('User not deleted');
        return deletedUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService(UserRepository);