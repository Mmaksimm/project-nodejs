const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    constructor(repository) {
        this.repository = repository;
    }

    getFighters() {
        const fighters = this.repository.getAll();
        if (!fighters) throw new Error('Fighters not found');
        return fighters;
    }

    getFighter(id) {
        const fighter = this.repository.getOne({ id });
        if (!fighter) throw new Error('Fighter not found');
        return fighter;
    }

    createFighter(FighterData) {
        const createdFighter = this.repository.create({
            ...FighterData,
            health: 100
        });
        if (!createdFighter) throw new Error('Fighter not created');
        return createdFighter;
    }

    updateFighter(id, FighterData) {
        if (!this.repository.getOne({ id })) {
            throw new Error("Fighter can't be updated, because not found");
        };
        const updatedFighter = this.repository.update(id, FighterData);
        if (!updatedFighter) throw new Error('Fighter not updated');
        return updatedFighter;
    }

    deleteFighter(id) {
        if (!this.repository.getOne({ id })) {
            throw new Error("Fighter can't be deleted, because not found");
        };
        const deletedFighter = this.repository.delete(id);
        if (!deletedFighter) throw new Error('Fighter not deleted');
        return deletedFighter;
    }
}

module.exports = new FighterService(FighterRepository);