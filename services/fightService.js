const { FightRepository } = require('../repositories/fightRepository');

class FightsService {
    // OPTIONAL TODO: Implement methods to work with fights
    constructor(repository) {
        this.repository = repository;
    }

    getFights() {
        const fights = this.repository.getAll();
        if (!fights) throw new Error('Fights not found');
        return fights;
    }

    getFight(id) {
        const fight = this.repository.getOne({ id });
        if (!fight) throw new Error('Fight not found');
        return fight;
    }

    createFight(FightData) {
        const createdFight = this.repository.create(FightData);
        if (!createdFight) throw new Error('Fight not created');
        return createdFight;
    }

    updateFight(id, FightData) {
        if (!this.repository.getOne({ id })) {
            throw new Error("Fight can't be updated, because not found");
        };
        const updatedFight = this.repository.update(id, FightData);
        if (!updatedFight) throw new Error('Fight not updated');
        return updatedFight;
    }

    deleteFight(id) {
        if (!this.repository.getOne({ id })) {
            throw new Error("Fight can't be deleted, because not found");
        };
        const deletedFight = this.repository.delete(id);
        if (!deletedFight) throw new Error('Fight not deleted');
        return deletedFight;
    }
}

module.exports = new FightsService(FightRepository);