function errors(err, message) {
  return ({
    message: err.message
      ? err.message
      : message,
    status: 404
  })
};

module.exports = errors;