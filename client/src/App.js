import React from 'react';
import StartScreen from './components/startScreen'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import './App.scss';

function App() {

  return (
    <MuiThemeProvider>
      <StartScreen />
    </MuiThemeProvider>
  );
}

export default App;
