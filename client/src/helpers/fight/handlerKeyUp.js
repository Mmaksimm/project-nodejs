import playerWhichPressKey from './playerWhichPressKey';

const handlerKeyUp = (players, event) => {
  event.preventDefault();

  const { playerPressKey = null } = playerWhichPressKey(players, event);
  if (!playerPressKey) return;

  const key = players[playerPressKey].fighterControlKeyNameFind(event);

  if (key.name === 'criticalHitCombination') {
    players[playerPressKey].keyUpCriticalHitCombination(event);
  };

  if (key.name === 'block') {
    players[playerPressKey].keyUpBlock();
  };

  if (key.name === 'attack') {
    players[playerPressKey].keyUpAttack();
  };
};

export default handlerKeyUp;
