class Player {
  constructor(fighter, figterPosition = 'left', block, attack, criticalHitCombination) {
    this.id = fighter.id;
    this.fighter = fighter;
    this.health = fighter.health;
    this.name = fighter.name
    this.attack = false;
    this.block = false;
    this.shot = 0;
    this.figterPosition = figterPosition;
    this.halthBarId = document.getElementById(`${figterPosition}-fighter-indicator`);
    this.keyBlock = block;
    this.keyAttack = attack;
    this.timeCriticalHitCombinationNotAllowed = 10000;
    this.criticalHitCombination = criticalHitCombination.map(key => (
      {
        name: key,
        value: false
      }));
    this.fighterControlKeys = [
      ...criticalHitCombination.map(key => (
        {
          name: 'criticalHitCombination',
          value: key
        })
      ),
      {
        name: 'block',
        value: block
      },
      {
        name: 'attack',
        value: attack
      }];
    this.criticalHitCombinationNotAllowed = false
  }

  criticalHitCombinationOk() {
    return this.criticalHitCombination.every(key => key.value);
  }

  criticalHitCombinationTest(event) {
    return this.criticalHitCombination.some(key => key.name === event.code && key.value === true);
  }

  criticalHitCombinationReset() {
    this.criticalHitCombination = this.criticalHitCombination.map(key => ({
      name: key.name,
      value: false
    }));
  }

  keyDownCriticalHitCombination(event) {
    this.keyCriticalHitCombination(event.code, true);
  }

  keyUpCriticalHitCombination(event) {
    this.keyCriticalHitCombination(event.code, false);
  }

  keyCriticalHitCombination(keyName, value) {
    this.criticalHitCombination = this.criticalHitCombination.map(key => ({
      name: key.name,
      value: key.name === keyName
        ? value
        : key.value
    }))
  }

  keyDownBlock() {
    this.block = true;
  }

  keyUpBlock() {
    this.block = false;
  }

  keyDownAttack() {
    this.attack = true;
  }

  keyUpAttack() {
    this.attack = false;
  }

  criticalHitCombinationNotAllowedSet() {
    this.criticalHitCombinationNotAllowed = true;
    setTimeout(() => {
      this.criticalHitCombinationNotAllowed = false;
    }, this.timeCriticalHitCombinationNotAllowed)
  }

  fighterControlKeyTest(event) {
    return this.fighterControlKeys.some(key => key.value === event.code);
  }

  fighterControlKeyNameFind(event) {
    return this.fighterControlKeys.find(key => key.value === event.code);
  }

  incrementShot() {
    this.shot += 1
  }

  calculationAndRenderHalthBar(damage) {
    this.health = this.health - damage;

    this.health = Math.max(0, this.health);
    this.halthBarId.style.width = (this.health / this.fighter.health * 100) + '%';
    return !this.health
  }
};

export default Player;