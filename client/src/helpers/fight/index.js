import controls from 'src/constants/controls'
import Player from './Player';
import handlerKeyDown from './handlerKeyDown';
import handlerKeyUp from './handlerKeyUp';

const fight = (firstFighter, secondFighter) => {
  return new Promise((resolve) => {
    const players = {
      left: new Player(
        firstFighter,
        'left',
        controls.PlayerOneBlock,
        controls.PlayerOneAttack,
        controls.PlayerOneCriticalHitCombination
      ),
      right: new Player(
        secondFighter,
        'right',
        controls.PlayerTwoBlock,
        controls.PlayerTwoAttack,
        controls.PlayerTwoCriticalHitCombination
      )
    }

    document.addEventListener('keydown', event => {
      const result = handlerKeyDown(players, event);
      if (result) {
        document.removeEventListener('keydown', handlerKeyDown);
        document.removeEventListener('keyup', handlerKeyUp);
        resolve(result)
      }
    });
    document.addEventListener('keyup', event => handlerKeyUp(players, event));
  })
};

export default fight;
