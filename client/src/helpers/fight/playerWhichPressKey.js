const playerWhichPressKey = (players, event) => {
  if (players.left.fighterControlKeyTest(event)) {
    return {
      playerPressKey: 'left',
      playerNotPressKey: 'right'
    }
  }
  if (players.right.fighterControlKeyTest(event)) {
    return {
      playerPressKey: 'right',
      playerNotPressKey: 'left'
    }
  }
  return {}
};

export default playerWhichPressKey;
