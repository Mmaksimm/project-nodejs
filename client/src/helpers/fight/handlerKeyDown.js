import getDamage from './getDamage';
import playerWhichPressKey from './playerWhichPressKey';

const handlerKeyDown = (players, event) => {
  event.preventDefault();
  const {
    playerPressKey = null,
    playerNotPressKey = null
  } = playerWhichPressKey(players, event);

  if (!playerPressKey) return;

  if (players[playerPressKey].block) return;
  if (players[playerPressKey].attack) return;

  const key = players[playerPressKey].fighterControlKeyNameFind(event);

  const damageСounting = damage => {
    if (players[playerNotPressKey].calculationAndRenderHalthBar(damage)) {
      return ({
        winner: players[playerPressKey],
        looser: players[playerNotPressKey]
      })
    };
  };

  if (key.name === 'criticalHitCombination') {
    if (players[playerPressKey].criticalHitCombinationTest(event)) return;
    players[playerPressKey].keyDownCriticalHitCombination(event);

    if (players[playerPressKey].criticalHitCombinationNotAllowed) return;
    if (!players[playerPressKey].criticalHitCombinationOk()) return;

    players[playerPressKey].criticalHitCombinationNotAllowedSet();
    players[playerPressKey].incrementShot();
    const damage = 2 * players[playerPressKey].fighter.attack;
    return damageСounting(damage);
  };

  if (players[playerPressKey].criticalHitCombination.some(key => key.value)) return;

  if (key.name === 'block') {
    players[playerPressKey].keyDownBlock();
  };

  if (key.name === 'attack') {
    players[playerPressKey].keyDownAttack();
    players[playerPressKey].incrementShot();
    if (players[playerNotPressKey].block) return;

    const damage = getDamage(
      players[playerPressKey].fighter,
      players[playerNotPressKey].fighter
    );
    return damageСounting(damage);
  };

}

export default handlerKeyDown;
