 const getHitPower = fighter => {
    // return hit power
    const { attack } = fighter;
    const criticalHitChance = Math.random() + 1;

    return attack * criticalHitChance
  }

  const getBlockPower = fighter => {
    // return block power
    const { defense } = fighter;
    const dodgeChance = Math.random() + 1;

    return defense * dodgeChance;
  }

  const getDamage = (attacker, defender) => {
    // return damage
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return Math.max(0, damage);
  }

  export default getDamage;