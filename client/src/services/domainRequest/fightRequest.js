import { get, post } from "../requestHelper";

const entity = 'fights';

export const getFights = async () => {
  const answer = await get(entity);
  return answer;
}

export const createFight = async (body) => {
  const answer = await post(entity, body);
  return answer;
}
