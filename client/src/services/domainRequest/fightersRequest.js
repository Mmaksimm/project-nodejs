import { get, post } from "../requestHelper";

const entity = 'fighters';

export const getFighters = async () => {
    const answer = await get(entity);
    return answer;
}

export const getFighter = async (id) => {
    const answer = await get(entity, id);
    return answer;
}

export const createFighter = async (body) => {
    const answer = await post(entity, body);
    return answer;
}
