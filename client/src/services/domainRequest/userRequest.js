import { post } from "../requestHelper";
const entity = 'users'

export const createUser = async (body) => {
    const answer = await post(entity, body);
    return answer;
}
