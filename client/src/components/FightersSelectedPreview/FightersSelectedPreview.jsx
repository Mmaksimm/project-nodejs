import React from 'react';
import FighterPreview from 'src/components/FighterPreview'
import Versus from 'src/components/Versus';

import './fightersSelectedPrewiew.scss'

const FightersSelectedPreview = ({ playerOne = false, playerTwo = false, startFight }) => (
  <div className="preview-container___root">
    <FighterPreview fighter={!playerOne ? false : playerOne} position="left" />
    {!playerOne
      ? ''
      : <Versus startFight={startFight} startFightView={!!playerOne && !!playerTwo} />
    }
    <FighterPreview fighter={!playerTwo ? false : playerTwo} position="right" />
  </div>
)

export default FightersSelectedPreview;
