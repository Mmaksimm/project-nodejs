import React, { Fragment } from 'react';
import SignInUpPage from 'src/components/signInUpPage';
import { isSignedIn } from 'src/services/authService';
import Game from 'src/components/Game';
import BattleResults from 'src/components/BattleResults';
import SignOut from 'src/components/signOut';
import { getFighter } from 'src/services/domainRequest/fightersRequest';

import './startScreen.scss';

class StartScreen extends React.Component {

    state = {
        isSignedIn: false,
        isRating: false,
        isMenu: true
    };

    fightersForDisplayDetail = new Map();

    getFighterForDisplay = async (fighter) => {
        if (this.fightersForDisplayDetail.has(fighter)) return this.fightersForDisplayDetail.get(fighter);
        const fighterForDisplayDetail = await getFighter(fighter);
        this.fightersForDisplayDetail.set(fighter, fighterForDisplayDetail);
        return fighterForDisplayDetail;
    }

    menuView(state) {
        if (state === this.state.isMenu) return;
        this.setState({
            isMenu: state
        })
    }

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState({ isSignedIn });
    }

    setFights = value => {
        this.setState({ fights: value });
    };

    render() {
        const { isSignedIn } = this.state;
        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn} />
        }
        return (
            <div className="startScreen">
                {this.state.isRating
                    ? <BattleResults getFighterForDisplay={this.getFighterForDisplay} />
                    : <Game menuView={this.menuView.bind(this)} />
                }
                {this.state.isMenu
                    ? (<Fragment>
                        <div className="startScreen-menu" onClick={() => this.setState({ isRating: !this.state.isRating })}>
                            {
                                this.state.isRating
                                    ? 'To the game'
                                    : 'To the rating'
                            }
                        </div>
                        <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)} />
                    </Fragment>
                    )
                    : ''
                }

            </div>
        )
    }
}

export default StartScreen;