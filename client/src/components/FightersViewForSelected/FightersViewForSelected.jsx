import React, { useState } from 'react';
import FightersSelectedPrewiew from 'src/components/FightersSelectedPreview';
import Fighter from 'src/components/Fighter';

import './fighters.scss';

function FightersView({ fighters, renderArena }) {
  const [playerOne, setPlayerOne] = useState(false);
  const [playerTwo, setPlayerTwo] = useState(false);

  const startFight = () => {
    renderArena(playerOne, playerTwo);
  };

  const selectFighter = fighter => {
    setPlayerOne(!playerOne ? fighter : playerOne);
    setPlayerTwo(!playerOne ? false : !playerTwo ? fighter : playerTwo);
  }

  return (
    <div className="fighters___root">
      <FightersSelectedPrewiew playerOne={playerOne} playerTwo={playerTwo} startFight={startFight} />
      <div className="fighters___list">
        {fighters.map((fighter) => <Fighter fighter={fighter} selectFighter={selectFighter} />)}
      </div>
    </div>
  )
}

export default FightersView;
