import React from 'react';

const FighterOnArena = ({ fighter, position }) => (
  <div className={`arena___fighter arena___${position}-fighter`}>
    <img
      src={fighter.source}
      title={fighter.name}
      alt={fighter.name}
      className="fighter-preview___img"
    />
  </div>
)

export default FighterOnArena;
