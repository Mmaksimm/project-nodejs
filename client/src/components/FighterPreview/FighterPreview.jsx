import React from 'react';

function FighterPreview({ fighter, position }) {
  if (!fighter) return null;
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  return (
    <div className={positionClassName}>
      <img src={fighter.source} title={fighter.name} alt={fighter.name} className="fighter-preview___img" />
      <pre className="fighter-preview__name">
        {Object.keys(fighter).map(key => {
          if (key === 'id' || key === 'source') return '';
          return `${key}: ${fighter[key]} \n`;
        }
        ).filter(el => el).join('')}
      </pre>
    </div>
  )
}

export default FighterPreview;