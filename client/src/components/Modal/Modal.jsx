import React from 'react';

import './modal.scss';

const Modal = ({ winner, hideModal }) => (
  <div className="modal-layer" >
    <div className="modal-root">
      <div className="modal-header">
        <span>{`${winner.name}'s winned! `}</span>
        <div className="close-btn" onClick={hideModal}>×</div>
      </div>
      <img src={winner.source} title={winner.name} alt={winner.name} />
    </div>
  </div>
)

export default Modal;
