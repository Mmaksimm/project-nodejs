import react from 'react';
import { renderArena } from 'src/components/arena';
import versusImg from 'src/resources/versus.png';
import { createFighterPreview } from 'src/components/fighterPreview';
import FighterPreview from 'src/components/FighterPreview';
import { fighterService } from 'src/services/fightersService';

function createFightersSelector(event, fighterId) {
  let selectedFighters = [];
  const fighter = async fighterId => await getFighterInfo(fighterId);
  const [playerOne, playerTwo] = selectedFighters;
  const firstFighter = playerOne ?? fighter;
  const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
  selectedFighters = [firstFighter, secondFighter];

  return (
    <RenderSelectedFighters selectedFighter={selectedFighter} />
  )
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId) {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (fighterDetailsMap.has(fighterId)) return { ...fighterDetailsMap.get(fighterId) };

  const fighterInfo = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterInfo);

  return { ...fighterInfo };
}

function RenderSelectedFighters(selectedFighters) {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;

  const VersusBlock = (selectedFighters) => {
    const canStartFight = selectedFighters.filter(Boolean).length === 2;
    const disabledBtn = canStartFight ? '' : 'disabled';

    return (
      <div className="preview-container___versus-block">
        <img className="preview-container___versus-img" src={versusImg} />
        <button className={`preview-container___fight-btn ${disabledBtn}`} onClick={() => startFight(selectedFighters)}>
          Fight
        </button>
      </div>
    )
  }

  return (
    < div className="preview-container___root" >
      <FighterPreview fighter={playerOne} ClassName="fighter-preview___root fighter-preview___left" />
      <VersusBlock />
      <FighterPreview fighter={playerTwo} ClassName="fighter-preview___root fighter-preview___right" />
    </div >
  )
}

function startFight(selectedFighters) {
  renderArena(selectedFighters);
}

export default FightersSelector
