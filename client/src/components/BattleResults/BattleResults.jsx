import React, { useState, useEffect } from 'react';
import { getFights } from 'src/services/domainRequest/fightRequest';

import './battleResults.scss';

const BattleResults = ({ getFighterForDisplay }) => {

  const [fights, setFights] = useState(false);

  const getFightsForDisplay = async () => {
    let fightsForDisplay = await getFights();
    const fightsForDisplayDetail = [];

    for (let i = fightsForDisplay.length - 1; i; i--) {
      const fightForDisplayDetail = [];

      fightForDisplayDetail[1] = await getFighterForDisplay(fightsForDisplay[i].fighter1);
      fightForDisplayDetail[2] = await getFighterForDisplay(fightsForDisplay[i].fighter2);

      const winnerNumber = fightsForDisplay[i].log[0].fighter1Health === 0
        ? 2
        : 1

      const loserNumber = winnerNumber === 1
        ? 2
        : 1

      const winner = {
        name: fightForDisplayDetail[winnerNumber].name,
        health: fightsForDisplay[i].log[0][`fighter${winnerNumber}Health`],
        shot: fightsForDisplay[i].log[0][`fighter${winnerNumber}Shot`],
        source: fightForDisplayDetail[winnerNumber].source
      };

      const loser = {
        name: fightForDisplayDetail[loserNumber].name,
        health: fightsForDisplay[i].log[0][`fighter${loserNumber}Health`],
        shot: fightsForDisplay[i].log[0][`fighter${loserNumber}Shot`],
        source: fightForDisplayDetail[loserNumber].source
      };

      fightsForDisplayDetail.push({
        winner,
        loser
      });
    };
    setFights(fightsForDisplayDetail);
  };



  useEffect(() => {
    if (!fights) getFightsForDisplay()
  });

  return fights
    ? (
      <div className="rating">
        <table className="rating-title">
          <thead>
            <th>Winner</th>
            <th>Loser</th>
          </thead>
        </table>
        <div className="rating-body-container">
          <table className="rating-body">
            <colgroup>
              <col className="winner"></col>
              <col className="loser"></col>
            </colgroup>
            {fights.map(fight => fight.winner && (
              <tr >
                <td className="winner">
                  <img src={fight.winner.source} title={fight.winner.name} alt={fight.winner.name} />
                  <div>{fight.winner.name}</div>
                  <div>shot {fight.winner.shot}</div>
                  <div>health {fight.winner.health}</div>
                </td>
                <td className="loser">
                  <img src={fight.loser.source} title={fight.loser.name} alt={fight.loser.name} />
                  <div>{fight.loser.name}</div>
                  <div>shot {fight.loser.shot}</div>
                  <div>health {fight.loser.health}</div>
                </td>
              </tr>
            ))}
          </table>
        </div>
      </div>
    )

    : (
      <div className="loading-overlay">
        <img src="logo.png" alt="loading" />
      </div>
    )
};

export default BattleResults;
