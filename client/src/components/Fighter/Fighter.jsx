import React from 'react';

const Fighter = ({ fighter, selectFighter }) => (
  <div
    className="fighters___fighter"
    onClick={() => selectFighter(fighter)}
  >
    <img
      src={fighter.source}
      title={fighter.name}
      alt={fighter.name}
      className="fighter___fighter-image"
    />
  </div>
)

export default Fighter;
