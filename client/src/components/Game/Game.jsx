import React, { useState, useEffect } from 'react';
import { getFighters } from 'src/services/domainRequest/fightersRequest'
import FightersViewForSelected from 'src/components/FightersViewForSelected';
import Arena from 'src/components/Arena';

import './game.scss';

function Game({ menuView }) {
  const [fighters, setFighters] = useState(false);
  const [selectedFighters, setSelectedFighters] = useState(false);

  const fightersForDisplay = async () => {
    const fightersForDisplay = await getFighters();
    setFighters(fightersForDisplay);
  };

  const onClose = () => {
    setSelectedFighters(false);
  }

  const renderArena = (...fighters) => {
    setSelectedFighters(fighters)
  }

  useEffect(() => {
    if (!fighters) fightersForDisplay()

    if (!selectedFighters) {
      menuView(true);
    } else {
      menuView(false)
    }
  });

  return (
    fighters
      ? selectedFighters
        ? <Arena selectedFighters={selectedFighters} onClose={onClose} />
        : <FightersViewForSelected fighters={fighters} renderArena={renderArena} />
      : <div className="loading-overlay">
        <img src="logo.png" alt="loading" />
      </div>
  )
}

export default Game;