import React from 'react';

const HealthIndicator = ({ name, position }) => (
  <div className="arena___fighter-indicator">
    <span className="arena___fighter-name">{name}</span>
    <div className="arena___health-indicator">
      <div className="arena___health-bar" id={`${position}-fighter-indicator`}></div>
    </div>
  </div>
)

export default HealthIndicator;
