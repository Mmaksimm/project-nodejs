import React from 'react';

import './versus.scss';
import versusImg from '../../resources/versus.png';

const Versus = ({ startFight, startFightView }) => (
  <div className="preview-container___versus-block">
    <img
      src={versusImg}
      className="preview-container___versus-img"
      alt="versus"

    />
    {startFightView
      ? (
        <button
          className="preview-container___fight-btn"
          onClick={startFight}
        >
          Fight
        </button>
      )
      : ''
    }

  </div>
)

export default Versus;
