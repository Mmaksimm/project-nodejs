import React, { useEffect, useState } from 'react';
import HealthIndicator from 'src/components/HealthIndicator';
import FighterOnArena from 'src/components/FighterOnArena';
import Modal from 'src/components/Modal';
import fight from 'src/helpers/fight';
import { createFight } from 'src/services/domainRequest/fightRequest';

import './arena.scss'

const Arena = ({ selectedFighters, onClose }) => {
  const [winner, setWinner] = useState(false);
  // todo:
  // - start the fight
  // - when fight is finished show winner
  useEffect(() => {
    fight(...selectedFighters)
      .then(({ winner, looser }) => {
        setWinner(winner.fighter);

        createFight({
          fighter1: winner.id,
          fighter2: looser.id,
          log: [
            {
              fighter1Shot: winner.shot,
              fighter2Shot: looser.shot,
              fighter1Health: Math.round(100 * winner.health) / 100,
              fighter2Health: Math.round(100 * looser.health) / 100
            }
          ]
        })
      });
  })

  const hideModal = () => {
    setWinner(false);
    onClose()
  }

  return (
    <div className="arena___root">
      <div className="arena___fight-status">
        <HealthIndicator name={selectedFighters[0].name} position="left" />
        <div className="arena___versus-sign"></div>
        <HealthIndicator name={selectedFighters[1].name} position="right" />
      </div>
      <div className="arena___battlefield">
        <FighterOnArena fighter={selectedFighters[0]} position="left" />
        <FighterOnArena fighter={selectedFighters[1]} position="right" />
      </div>
      {winner
        ? <Modal winner={winner} hideModal={hideModal} />
        : ''
      }
    </div>
  )
}

export default Arena;
