const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const errors = require('../helpers/errors');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router
  .get('/', (req, res, next) => {
    try {
      res.data = FightService.getFights();
    } catch (err) {
      res.err = errors(err, 'Fights not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .get('/:id', (req, res, next) => {
    try {
      res.data = FightService.getFight(req.params.id);
    } catch (err) {
      res.err = errors(err, 'Fight not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .post('/', createFightValid, (req, res, next) => {
    try {
      if (res.err) next();
      res.data = FightService.createFight(req.body);
    } catch (err) {
      res.err = errors(err, 'Fight not created.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .put('/:id', /*updateFightValid,*/(req, res, next) => {
    try {
      if (res.err) next();
      res.data = FightService.updateFight(req.params.id, req.body);
    } catch (err) {
      res.err = errors(err, 'Fight not updated.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .delete('/:id', (req, res, next) => {
    try {
      res.data = FightService.deleteFight(req.params.id);
    } catch (err) {
      res.err = errors(err, 'Fight not deleted.');
    } finally {
      next();
    }
  }, responseMiddleware)

module.exports = router;
