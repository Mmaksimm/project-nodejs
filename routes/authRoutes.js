const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const errors = require('../helpers/errors');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        data = AuthService.login(req.body);
        if (!data) throw new Error('Login is incorrect.')
        res.data = data;

    } catch (err) {
        res.err = errors(err, 'Login is incorrect.');
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
