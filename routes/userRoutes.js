const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const errors = require('../helpers/errors');

const router = Router();

// TODO: Implement route controllers for user
router
  .get('/', (req, res, next) => {
    try {
      res.data = UserService.getUsers();
    } catch (err) {
      res.err = errors(err, 'Users not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .get('/:id', (req, res, next) => {
    try {
      res.data = UserService.getUser(req.params.id);
    } catch (err) {
      res.err = errors(err, 'User not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .post('/', createUserValid, (req, res, next) => {
    try {
      if (res.err) next();
      res.data = UserService.createUser(req.body);
    } catch (err) {
      res.err = errors(err, 'User not created.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .put('/:id', updateUserValid, (req, res, next) => {
    try {
      if (res.err) next();
      res.data = UserService.updateUser(req.params.id, req.body);
    } catch (err) {
      res.err = errors(err, 'User not updated.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .delete('/:id', (req, res, next) => {
    try {
      res.data = UserService.deleteUser(req.params.id);
    } catch (err) {
      res.err = errors(err, 'User not deleted.');
    } finally {
      next();
    }
  }, responseMiddleware)

module.exports = router;
