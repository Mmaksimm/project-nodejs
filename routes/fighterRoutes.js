const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const errors = require('../helpers/errors');

const router = Router();

// TODO: Implement route controllers for fighter
router
  .get('/', (req, res, next) => {
    try {
      res.data = FighterService.getFighters();
    } catch (err) {
      res.err = errors(err, 'Fighters not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .get('/:id', (req, res, next) => {
    try {
      res.data = FighterService.getFighter(req.params.id);
    } catch (err) {
      res.err = errors(err, 'Fighter not fond.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .post('/', createFighterValid, (req, res, next) => {
    try {
      if (res.err) next();
      res.data = FighterService.createFighter(req.body);
    } catch (err) {
      res.err = errors(err, 'Fighter not created.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .put('/:id', updateFighterValid, (req, res, next) => {
    try {
      if (res.err) next();
      res.data = FighterService.updateFighter(req.params.id, req.body);
    } catch (err) {
      res.err = errors(err, 'Fighter not updated.');
    } finally {
      next();
    }
  }, responseMiddleware)

  .delete('/:id', (req, res, next) => {
    try {
      res.data = FighterService.deleteFighter(req.params.id);
    } catch (err) {
      res.err = errors(err, 'Fighter not deleted.');
    } finally {
      next();
    }
  }, responseMiddleware)

module.exports = router;
