const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        const body = req.body
        const bodyKeys = Object.keys(body);
        const modelKeys = Object.keys(fighter);

        if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
        fieldEmptyValidation(bodyKeys, modelKeys);
        nameValidation(body.name);
        powerValidation(body.power);
        defenseValidation(body.defense);

    } catch (err) {
        const message = err.message
            ? err.message
            : 'Fighter not created.'

        res.err = {
            status: 400,
            message: message
        };
    } finally {
        next()
    };
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        const body = req.body
        const bodyKeys = Object.keys(body);
        const modelKeys = Object.keys(fighter);

        if (!bodyKeys.length) throw new Error('Bad request');
        if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
        if (bodyKeys.includes('name') && nameValidation(body.name));
        if (bodyKeys.includes('power') && powerValidation(body.power));
        if (bodyKeys.includes('defense') && defenseValidation(body.defense));

    } catch (err) {
        const message = err.message
            ? err.message
            : 'Fighter not updated.'

        res.err = {
            status: 400,
            message: message
        };
    } finally {
        next();
    };
}

function badRequestValidation(modelKeys, bodyKeys) {
    return !bodyKeys.every(bodyKey => bodyKey !== 'id' && bodyKey !== 'health' && modelKeys.includes(bodyKey));
};

function fieldEmptyValidation(bodyKeys, modelKeys) {
    if ((modelKeys.length - 2) !== (bodyKeys.length)) {
        const message = modelKeys.filter(key => {
            return (key !== 'id') && (key !== 'health') && !bodyKeys.includes(key);
        }).join(', ');
        throw new Error(`${message} field is empty`)
    };
};

function nameValidation(name) {
    if (!(/^\w{3,}$/.test(name))) {
        throw new Error('The fighter name name is incorrect')
    };
};

function powerValidation(power) {
    if (1 <= power && power <= 100) return;
    throw new Error('Please enter a power from 1 to 100')
};

function defenseValidation(defense) {
    if (1 <= defense && defense <= 10) return;
    throw new Error('Please enter a defense from 1 to 10')
};


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;