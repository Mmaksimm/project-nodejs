const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    try {
        const body = req.body
        const bodyKeys = Object.keys(body);
        const modelKeys = Object.keys(user);

        if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
        fieldEmptyValidation(bodyKeys, modelKeys);
        nameValidation(body.firstName, 'first name');
        nameValidation(body.lastName, 'last name');
        emailValidation(body.email);
        phoneValidation(body.phoneNumber)
        passwordValidation(body.password)

    } catch (err) {
        const message = err.message
            ? err.message
            : 'Accaunt not created.'

        res.err = {
            status: 400,
            message: message
        };
    } finally {
        next()
    };
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        const body = req.body
        const bodyKeys = Object.keys(body);
        const modelKeys = Object.keys(user);

        if (!bodyKeys.length) throw new Error('Bad request');
        if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
        if (bodyKeys.includes('firstName') && nameValidation(body.firstName, 'first name'));
        if (bodyKeys.includes('lastName') && nameValidation(body.lastName, 'last name'));
        if (bodyKeys.includes('email') && emailValidation(body.email));
        if (bodyKeys.includes('phoneNumber') && phoneValidation(body.phoneNumber));
        if (bodyKeys.includes('password') && passwordValidation(body.password));

    } catch (err) {
        const message = err.message
            ? err.message
            : 'Accaunt not updated.'

        res.err = {
            status: 400,
            message: message
        };
    } finally {
        next();
    };
}

function badRequestValidation(modelKeys, bodyKeys) {
    return !bodyKeys.every(bodyKey => bodyKey !== 'id' && modelKeys.includes(bodyKey));
};

function fieldEmptyValidation(bodyKeys, modelKeys) {
    if ((modelKeys.length - 1) !== (bodyKeys.length)) {
        const message = modelKeys.filter(key => {
            return (key !== 'id') && !bodyKeys.includes(key);
        }).join(', ');
        throw new Error(`${message} field is empty`)
    };
};

function nameValidation(name, nameMessage) {
    if (!(/^\w{3,}$/.test(name))) {
        throw new Error(`The ${nameMessage} name is incorrect`)
    };
};

function emailValidation(email) {
    if (!(/^\w+@gmail.com$/.test(email))) {
        throw new Error('Please enter your email address in the format login@gmail.com')
    };
};

function phoneValidation(phoneNumber) {
    if (!(/^\+380\d{9}$/.test(phoneNumber))) {
        throw new Error('Please enter the telephone number in the format + 380xxxxxxxxx.')
    };
}

function passwordValidation(password) {
    if (password.trim().length < 3) {
        throw new Error('The password must be at least 3 characters long')
    };
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;