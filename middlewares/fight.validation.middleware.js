const { fight } = require('../models/fight');

const createFightValid = (req, res, next) => {
  try {
    const body = req.body
    const bodyKeys = getKeys(body);
    const modelKeys = getKeys(fight);

    if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
    fieldEmptyValidation(bodyKeys, modelKeys);
    if (checkNumber(body)) throw new Error('Bad request');

  } catch (err) {
    const message = err.message
      ? err.message
      : 'Fighter not created.'

    res.err = {
      status: 400,
      message: message
    };
  } finally {
    next()
  };
}

const updateFightValid = (req, res, next) => {
  try {
    const body = req.body
    const bodyKeys = getKeys(body);
    const modelKeys = getKeys(fight);

    if (!bodyKeys.length) throw new Error('Bad request');
    if (badRequestValidation(modelKeys, bodyKeys)) throw new Error('Bad request');
    if (checkNumber(body)) throw new Error('Bad request');

  } catch (err) {
    const message = err.message
      ? err.message
      : 'Fighter not updated.'

    res.err = {
      status: 400,
      message: message
    };
  } finally {
    next();
  };
}

function getKeys(obj) {
  let answer = [];
  let keys = Object.keys(obj);
  keys.map(key => {
    switch (typeof obj[key]) {
      case 'string' || 'number':
        answer = [...answer, key];
        break;

      case 'object':
        if (!Array.isArray(obj[key])) throw new Error('Bad request');
        const newKey = obj[key].forEach((element, number) => {
          if (typeof element !== 'object' || number !== 0) {
            throw new Error('Bad request');
          };

          const newKey = Object.keys(element);
          answer = [...answer, key, ...newKey]
        });
        break;

      default:
        throw new Error('Bad request');
        break;
    }
  })

  return answer;
};

function badRequestValidation(modelKeys, bodyKeys) {
  return !bodyKeys.every(bodyKey => bodyKey !== 'id' && modelKeys.includes(bodyKey));
};

function fieldEmptyValidation(bodyKeys, modelKeys) {
  if ((modelKeys.length - 1) !== (bodyKeys.length)) {
    const message = modelKeys.filter(key => {
      return (key !== 'id') && !bodyKeys.includes(key);
    }).join(', ');
    throw new Error(`${message} field is empty`)
  };
};

function checkNumber(body) {
  return Object.values(body.log[0]).some(elem => !(Object.prototype.toString.call(elem) === '[object Number]'))
}

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;