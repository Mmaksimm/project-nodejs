const { json } = require("express");

const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.err) {
        const { status, message } = res.err;
        res.status(status).json({
            error: true,
            message
        })
    };

    res.status(200).
        json(res.data);
}

exports.responseMiddleware = responseMiddleware;